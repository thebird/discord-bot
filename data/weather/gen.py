#[Greeting adj][Greeting][Name]. I'm [adj - happy] to [adj - assist] you.
#[sim - Today will][weather phrase][greeting adj][clouds/sun/rain/snow] today.
#Temperature is currently at a [descriptive word][temp in both C and freedom units).
#[adj happy to help and have a fantastic day]
from random import randint

greeting_adj = []
greeting = []
name = []
adj_happy = []
adj_assist = []
weather_phrase = []
temp_low = []
temp_mid = []
temp_high = []

with open('greeting-adj') as f:
    greeting_adj = [line.strip() for line in f.readlines()]

with open('greeting') as f:
    greeting = [line.strip() for line in f.readlines()]

with open('name') as f:
    name = [line.strip() for line in f.readlines()]

with open('adj-happy') as f:
    adj_happy = [line.strip() for line in f.readlines()]

with open('adj-assist') as f:
    adj_assist = [line.strip() for line in f.readlines()]

with open('weather-phrase') as f:
    weather_phrase = [line.strip() for line in f.readlines()]


with open('phrases', 'w') as f:
    for i in range(5000):
        sent = ''
        sent += greeting_adj[randint(0, len(greeting_adj)-1)].capitalize()
        sent += ' ' + greeting[randint(0, len(greeting)-1)]
        sent += ' ' + name[randint(0, len(name)-1)] + '! '

        sent += 'I am ' + adj_happy[randint(0, len(adj_happy)-1)]
        sent += ' to ' + adj_assist[randint(0, len(adj_assist)-1)] + ' you! :) '

        sent += 'The weather today ' + weather_phrase[randint(0, len(weather_phrase)-1)]
        sent += ' ' + greeting_adj[randint(0, len(greeting_adj)-1)].lower() + ' $WEATHER$. At the moment the '

        sent += 'temperature is at a $TEMP$'

        f.write(sent + '\n')





