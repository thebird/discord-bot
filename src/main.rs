#[macro_use]
extern crate automate;

use std::env;
use dotenv::dotenv;
use automate::{ Error, Configuration, Context, Automate };
use automate::gateway::MessageCreateDispatch;
use automate::http::CreateMessage;
use openweather::OpenWeather;
use std::boxed::Box;
use rand::seq::SliceRandom;
use automate::events::{Initializable, StatefulListener};
use std::{
    fs::File,
    io::{BufRead, BufReader},
};


#[derive(State, Clone)]
struct WeatherReport {
    phrases: Vec<String>,
    cold: Vec<String>,
    temperate: Vec<String>,
    hot: Vec<String>,
}
   

impl Default for WeatherReport {

    fn default() -> WeatherReport {
        let file = File::open("data/weather/phrases")
            .unwrap_or_else(|_| panic!("File not found."));
        let buf = BufReader::new(file);
        let phrases = buf.lines().map(|l| l.expect("Couldn't read line")).collect();
        WeatherReport { 
            phrases: phrases,
            cold: vec!["brisk".into(), "cold".into(), "chilly".into(), "crisp".into()],
            temperate: vec!["comfortable".into(), "perfect".into(), "amenable".into(), "satisfactory".into()],
            hot: vec!["hot".into(), "boiling".into(), "scorching".into(), "sizzling".into()],
        }
    }

}


impl Initializable for WeatherReport {

    fn initialize() -> Vec<StatefulListener<Self>> {
        methods!(WeatherReport: get_report)
    }

}


impl WeatherReport {

    async fn process_response<'a>(&mut self, response: &'a str) -> Result<(&'a str, &'a str), Error> {
        Ok(match response.chars().count() {
                d if d < 4 => ("", ""),
                _ => (&response[0..2], &response[2..].trim())
            }
        )
    }


    fn get_weather_description(&self, description: &String) -> &str {
        match description {
            d if d.contains("cloud") => "cloudy wonder",
            d if d.contains("sun") => "sunny joy",
            d if d.contains("rain") => "rainy delight",
            d if d.contains("snow") => "snowy majesty",
            _ => "ERROR",
        }
    }


    fn get_temperature_description(&self, temp: f32) -> &str {
        let temp_description = match temp {
            t if t < 15.0 => self.cold.choose(&mut rand::thread_rng()),
            t if t <= 26.0 => self.temperate.choose(&mut rand::thread_rng()),
            _ => self.hot.choose(&mut rand::thread_rng()),
        };

        match temp_description {
            Some(t) => t,
            None => "ERROR",
        }
    }


    async fn get_statement(&mut self, weather: OpenWeather) -> Result<String, Error> {
        let mut statement = match self.phrases.choose(&mut rand::thread_rng()) {
            Some(s) => s.to_string(),
            None => "".to_string(),
        };

        let temp_c: f32 = weather.main.temp;
        let temp_f: f32 = temp_c * 9.0/5.0 + 32.0;

        let weather_description = self.get_weather_description(&weather.weather[0].description);
        let temp_description = self.get_temperature_description(temp_c);

        statement = statement.replace("$TEMP$", temp_description);
        statement = statement.replace("$WEATHER$", weather_description);
        statement = format!("{} {:.1}°C, which is {:.1}°Freedoms.", statement, temp_c, temp_f);
        Ok(statement.into())
    }
    

    #[listener]
    async fn get_report(&mut self, ctx: &mut Context, data: &MessageCreateDispatch) -> Result<(), Box<dyn Error>> {
        let response = &data.0.content;
        let (cmd, msg) = self.process_response(&response).await?;


        if cmd == "?w" {
            let weather: OpenWeather = match OpenWeather::get_by_city(&msg).await {
                Ok(data) => data,
                Err(_) => OpenWeather::default(),
            };

            let mut content = Some(format!("Sorry, but I couldn't find that."));

            if weather.id != -1 {
                content = match self.get_statement(weather).await {
                    Ok(data) => Some(data),
                    Err(_) => Some(format!("Error processing your request.")),
                };
            }

            ctx.create_message(data.0.channel_id, CreateMessage {
                content,
                ..Default::default()
            }).await?;
        }
        Ok(())
    }

}


fn main() {
    dotenv().expect("No env file found");
    let token = env::var("DISCORD_KEY").unwrap();
    let config = Configuration::new(token)
        .register(stateful!(WeatherReport::default()));
    Automate::launch(config);
}




